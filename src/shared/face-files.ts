export const fileNames = [
  "face_angry",
  "face_crying",
  "face_frighten",
  "face_happy",
  "face_laughing",
  "face_normal",
  "face_sad",
  "face_surprise",
  "glasses_circle",
  "glasses_square",
  "sunglasses",
];
