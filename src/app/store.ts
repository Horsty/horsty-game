import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import carouselReducer from "../features/Carousel/carouselSlice";
import featureReducer from "../features/Feature/featureSlice";
import avatarReducer from "../features/Avatar/avatarSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    carousel: carouselReducer,
    feature: featureReducer,
    avatar: avatarReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
