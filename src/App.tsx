import { GTCharacter, GTInteractiveScreen } from "@horsty/game-tools";
import { useState } from "react";
import styles from "./App.module.css";
import { useAppSelector } from "./app/hooks";
import Avatar, {
  getAccessory,
  getFace,
  getHairCut,
  getLegs,
  getPetItem,
  getStyleItem,
  getTop,
} from "./features/Avatar/Avatar";
import { selectAvatar } from "./features/Avatar/avatarSlice";
import Carousel, { IItems } from "./features/Carousel/Carousel";
import Feature from "./features/Feature/Feature";
import { selectFeature } from "./features/Feature/featureSlice";
import { fileNames as facesLocation } from "./shared/face-files";
import { fileNames as haircutLocation } from "./shared/haircut-files";
import { fileNames as accessoryLocation } from "./shared/accessory-files";
import { fileNames as legsLocation } from "./shared/legs-file";
import { fileNames as topsLocation } from "./shared/top-files";
import { fileNames as styleLocation } from "./shared/style-files";
import { fileNames as petLocation } from "./shared/pet-files";

const App = () => {
  const [startGame, setStartGame] = useState(false);
  const defaultValue = "haircuts";
  const selectedItem = useAppSelector(selectFeature);

  const avatar = useAppSelector(selectAvatar);
  const spritesheet = "/assets/spritesheets/shared/basic_character.png";
  const items: IItems = {
    haircuts: haircutLocation,
    faces: facesLocation,
    accessories: accessoryLocation,
    legs: legsLocation,
    tops: topsLocation,
    styles: styleLocation,
    pets: petLocation,
  };

  const avatarSetup: any = {
    direction: "",
    move: "",
    spritesheet,
    haircut: getHairCut(avatar.selectedHaircut, items),
    face: getFace(avatar.selectedFace, items),
    clothes: getTop(avatar.selectedTop, items),
    pants: getLegs(avatar.selectedLeg, items),
    accessory: getAccessory(avatar.selectedAccessory, items),
    styleItem: getStyleItem(avatar.selectedStyleItem, items),
    pet: getPetItem(avatar.selectedPet, items),
  };

  return (
    <div className={styles.container}>
      {startGame ? (
        <>
          <GTInteractiveScreen
            background={"./assets/images/CameraDemoMap.png"}
            player={() => GTCharacter({ ...avatarSetup })}
          />
          <button
            className={`${styles.goBack} nes-btn`}
            onClick={() => setStartGame(false)}
          >
            Go back
          </button>
        </>
      ) : (
        <>
          <section className={styles.avatar}>
            <Avatar />
          </section>
          <section className={styles.feature}>
            <Feature defaultValue={defaultValue} />
          </section>
          <section className={styles.carousel}>
            <Carousel selectedItem={selectedItem} />
          </section>
          <button className="nes-btn" onClick={() => setStartGame(true)}>
            Start the game
          </button>
        </>
      )}
    </div>
  );
};

export default App;
