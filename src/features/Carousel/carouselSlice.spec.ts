import carouselReducer, {
  CarouselState,
  increment,
  decrement,
  incrementByAmount,
} from "./carouselSlice";

describe("carousel reducer", () => {
  const initialState: CarouselState = {
    value: 3,
    status: "idle",
  };
  it("should handle initial state", () => {
    expect(carouselReducer(undefined, { type: "unknown" })).toEqual({
      value: 0,
      status: "idle",
    });
  });

  it("should handle increment", () => {
    const actual = carouselReducer(initialState, increment());
    expect(actual.value).toEqual(4);
  });

  it("should handle decrement", () => {
    const actual = carouselReducer(initialState, decrement());
    expect(actual.value).toEqual(2);
  });

  it("should handle incrementByAmount", () => {
    const actual = carouselReducer(initialState, incrementByAmount(2));
    expect(actual.value).toEqual(5);
  });
});
