import { GTCharacter } from "@horsty/game-tools";
import styles from "./Carousel.module.css";
import { fileNames as facesLocation } from "./../../shared/face-files";
import { fileNames as haircutLocation } from "./../../shared/haircut-files";
import { fileNames as accessoryLocation } from "./../../shared/accessory-files";
import { fileNames as legsLocation } from "./../../shared/legs-file";
import { fileNames as topsLocation } from "./../../shared/top-files";
import { fileNames as styleLocation } from "./../../shared/style-files";
import { fileNames as petLocation } from "./../../shared/pet-files";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { decrement, increment, init, selectChoice } from "./carouselSlice";
import { useEffect } from "react";

const Carousel = ({ selectedItem = "haircuts" }) => {
  const dispatch = useAppDispatch();
  const extension = "png";
  const items: IItems = {
    haircuts: haircutLocation,
    faces: facesLocation,
    accessories: accessoryLocation,
    legs: legsLocation,
    tops: topsLocation,
    styles: styleLocation,
    pets: petLocation,
  };

  useEffect(() => {
    dispatch(init(items[selectedItem].length - 1));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedItem]);

  const choice = useAppSelector(selectChoice);
  
  const getNext = (index: number, items: IItems) => {
    // last item is choose so next is the first
    const target = index === items[selectedItem].length - 1 ? -1 : index;
    return getItemSprite(target + 1, items);
  };
  const getPrevious = (index: number, items: IItems) => {
    // first item is choose so previous is the last
    const target = index < 1 ? items[selectedItem].length : index;
    return getItemSprite(target - 1, items);
  };
  const getItemSprite = (index: number, items: IItems) => {
    return `/assets/spritesheets/${selectedItem}/${items[selectedItem][index]}.${extension}`;
  };

  return (
    <div className={styles.carousel}>
      <div className={styles.carousel_item}>
        <div className={`${styles.before} ${styles.block}`}>
          <div className="nes-container is-rounded">
            <GTCharacter
              haircut={getPrevious(choice, items)}
              withShadow={false}
            />
          </div>
          <button className="nes-btn" onClick={() => dispatch(decrement())}>
            Item précédent
          </button>
        </div>
        <div className={`${styles.primary} ${styles.block}`}>
          <GTCharacter
            spritesheet={"/assets/spritesheets/shared/basic_character.png"}
            haircut={getItemSprite(choice, items)}
            withShadow={false}
          />
        </div>
        <div className={`${styles.next} ${styles.block}`}>
          <div className="nes-container is-rounded">
            <GTCharacter haircut={getNext(choice, items)} withShadow={false} />
          </div>
          <button className="nes-btn" onClick={() => dispatch(increment())}>
            Item suivant
          </button>
        </div>
      </div>
      {/* <button className={`nes-btn ${styles.button}`}>Valider mon choix</button> */}
    </div>
  );
};

export default Carousel;
export interface IItems {
  [key: string]: string[];
  haircuts: string[];
  faces: string[];
  accessories: string[];
  legs: string[];
  styles: string[];
  tops: string[];
  pets: string[];
}
export enum ItemType {
  "haircuts",
}
