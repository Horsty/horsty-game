import { GTCharacter } from "@horsty/game-tools";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { IItems } from "../Carousel/Carousel";
import { selectChoice } from "../Carousel/carouselSlice";

import { fileNames as facesLocation } from "./../../shared/face-files";
import { fileNames as haircutLocation } from "./../../shared/haircut-files";
import { fileNames as accessoryLocation } from "./../../shared/accessory-files";
import { fileNames as legsLocation } from "./../../shared/legs-file";
import { fileNames as topsLocation } from "./../../shared/top-files";
import { fileNames as styleLocation } from "./../../shared/style-files";
import { fileNames as petLocation } from "./../../shared/pet-files";
import { changeSprite, selectAvatar } from "./avatarSlice";
import { useEffect } from "react";
import { selectFeature } from "../Feature/featureSlice";

const Avatar = () => {
  const dispatch = useAppDispatch();
  const choice = useAppSelector(selectChoice);
  const selectedFeature = useAppSelector(selectFeature);
  useEffect(() => {
    dispatch(changeSprite({ feature: selectedFeature, choice }));
    // eslint-disable-next-line
  }, [choice, selectFeature]);
  const avatar = useAppSelector(selectAvatar);
  const items: IItems = {
    haircuts: haircutLocation,
    faces: facesLocation,
    accessories: accessoryLocation,
    legs: legsLocation,
    tops: topsLocation,
    styles: styleLocation,
    pets: petLocation,
  };
  return (
    <GTCharacter
      // move={true}
      // direction={"left"}
      spritesheet={"/assets/spritesheets/shared/basic_character.png"}
      haircut={getHairCut(avatar.selectedHaircut, items)}
      face={getFace(avatar.selectedFace, items)}
      styleItem={getStyleItem(avatar.selectedStyleItem, items)}
      pants={getLegs(avatar.selectedLeg, items)}
      clothes={getTop(avatar.selectedTop, items)}
      accessory={getAccessory(avatar.selectedAccessory, items)}
      pet={getPetItem(avatar.selectedPet, items)}
    />
  );
};

export default Avatar;

export const getHairCut = (index: number, items: IItems) => {
  return `/assets/spritesheets/haircuts/${items.haircuts[index]}.png`;
};
export const getFace = (index: number, items: IItems) => {
  return `/assets/spritesheets/faces/${items.faces[index]}.png`;
};
export const getTop = (index: number, items: IItems) => {
  return `/assets/spritesheets/tops/${items.tops[index]}.png`;
};
export const getAccessory = (index: number, items: IItems) => {
  return `/assets/spritesheets/accessories/${items.accessories[index]}.png`;
};
export const getLegs = (index: number, items: IItems) => {
  return `/assets/spritesheets/legs/${items.legs[index]}.png`;
};
export const getStyleItem = (index: number, items: IItems) => {
  return `/assets/spritesheets/styles/${items.styles[index]}.png`;
};
export const getPetItem = (index: number, items: IItems) => {
  return `/assets/spritesheets/pets/${items.pets[index]}.png`;
};
