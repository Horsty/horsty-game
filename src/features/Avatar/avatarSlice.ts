import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface AvatarState {
  selectedHaircut: number;
  selectedFace: number;
  selectedLeg: number;
  selectedTop: number;
  selectedAccessory: number;
  selectedStyleItem: number;
  selectedPet: number;
}

const initialState: AvatarState = {
  selectedHaircut: 0,
  selectedFace: 0,
  selectedLeg: 0,
  selectedAccessory: 0,
  selectedTop: 0,
  selectedStyleItem: 0,
  selectedPet: 0,
};

export const avatarSlice = createSlice({
  name: "avatar",
  initialState,
  reducers: {
    changeSprite: (
      state,
      action: PayloadAction<{ feature: string; choice: number }>
    ) => {
      const lastValue = action.payload.choice;
      switch (action.payload.feature) {
        case "haircuts":
          state.selectedHaircut = lastValue;
          break;
        case "faces":
          state.selectedFace = lastValue;
          break;
        case "legs":
          state.selectedLeg = lastValue;
          break;
        case "styles":
          state.selectedStyleItem = lastValue;
          break;
        case "accessories":
          state.selectedAccessory = lastValue;
          break;
        case "tops":
          state.selectedTop = lastValue;
          break;
        case "pets":
          state.selectedPet = lastValue;
          break;
      }
    },
  },
});
export const { changeSprite } = avatarSlice.actions;
export const selectAvatar = (state: RootState) => state.avatar;
export default avatarSlice.reducer;
