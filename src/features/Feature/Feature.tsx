import { BaseSyntheticEvent, useEffect } from "react";
import { useAppDispatch } from "../../app/hooks";
import { setSelectedValue } from "./featureSlice";

const Feature = ({ defaultValue = "haircuts" }) => {
  const dispatch = useAppDispatch();

  const onChange = (event: BaseSyntheticEvent) => {
    dispatch(setSelectedValue(event.target.value));
  };

  useEffect(() => {
    dispatch(setSelectedValue(defaultValue));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <label htmlFor="default_select">Choose your custom</label>
      <div className="nes-select">
        <select
          required
          id="default_select"
          defaultValue={"haircuts"}
          onChange={onChange}
        >
          <option value="haircuts">Haircuts</option>
          <option value="accessories">Accessories</option>
          <option value="legs">Legs</option>
          <option value="styles">Style items</option>
          <option value="tops">Tops</option>
          <option value="faces">Faces</option>
          <option value="pets">Pets</option>
        </select>
      </div>
    </>
  );
};

export default Feature;
