import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface FeatureState {
  selectedFeature: string;
}

const initialState: FeatureState = {
  selectedFeature: "haircuts",
};

export const featureSlice = createSlice({
  name: "feature",
  initialState,
  reducers: {
    setSelectedValue: (state, action: PayloadAction<string>) => {
      state.selectedFeature = action.payload;
    },
  },
});

export const { setSelectedValue } = featureSlice.actions;
export const selectFeature = (state: RootState) =>
  state.feature.selectedFeature;

export default featureSlice.reducer;
